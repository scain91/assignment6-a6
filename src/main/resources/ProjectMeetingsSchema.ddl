create table projects(name varchar(255) NOT NULL, project_description varchar(255) NOT NULL,
  project_id int NOT NULL AUTO_INCREMENT, PRIMARY KEY(project_id));

create table meetings(name varchar(255) NOT NULL, year VARCHAR(4) NOT NULL, meetings_id int AUTO_INCREMENT,
  project_id int NOT NULL, PRIMARY KEY(meetings_id), FOREIGN KEY(project_id) REFERENCES projects(project_id));

insert into projects(name, project_description) values("Project 1", "Project Description 1");

insert into meetings(name,year,project_id) values("Meeting 1", "2014", 1);

insert into meetings(name, year, project_id) values("Meeting 4", "2015",
                                             (select project_id from projects
                                             where project_description="Project Description 1"));

select * from (meetings join projects on meetings.project_id = projects.project_id)
where projects.project_description="Project Description 1";

select * from (meetings join projects on meetings.project_id = projects.project_id);

select * from (meetings left outer join projects on meetings.project_id = projects.project_id);

select * from (meetings right outer join projects on meetings.project_id = projects.project_id);