package assign.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

/**
 * Created by Sara on 4/27/2015.
 */
@XmlRootElement(name = "meetings")
@XmlAccessorType(XmlAccessType.FIELD)
public class Meetings {

    private List<Meeting> meetings;
    private int count;

    public Meetings() {
        // this form used by Hibernate
    }

    public Meetings(List<Meeting> meetings) {
        this.meetings = meetings;
    }

    public Meetings(List<Meeting> meetings, int count) {
        this.meetings = meetings;
        this.count = count;
    }

    public List<Meeting> getMeetings() {
        return meetings;
    }

    public void setMeetings(List<Meeting> meetings) {
        this.meetings = meetings;
    }

    @XmlTransient
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
