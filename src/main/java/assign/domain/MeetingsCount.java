package assign.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Sara on 4/27/2015.
 */
@XmlRootElement(name = "meetings")
@XmlAccessorType(XmlAccessType.FIELD)
public class MeetingsCount {

    private int count;

    public MeetingsCount() {
        // this form used by Hibernate
        this.count = 0;
    }

    public MeetingsCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
