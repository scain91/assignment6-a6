package assign.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Created by Sara on 4/27/2015.
 */
@Entity
@Table( name = "meetings" ) //todo: should this be here or in Meetings?
@XmlRootElement(name = "meeting")
public class Meeting {

    private Long id;

    private String team_meeting_name;
    private String year;
    private String meeting_name; // course or something else
    private String link;

    public Meeting() {
        // this form used by Hibernate
    }

    public Meeting(String team_meeting_name, String year, String meeting_name, String link) {
        // for application use, to create new meeting instances
        this.team_meeting_name = team_meeting_name;
        this.year = year;
        this.meeting_name = meeting_name;
        this.link = link;
    }

    public Meeting(String team_meeting_name, String year, String meeting_name, String link, long providedId) {
        // for application use, to create new meeting instances
        this.team_meeting_name = team_meeting_name;
        this.year = year;
        this.meeting_name = meeting_name;
        this.link = link;
        this.id = providedId;
    }

    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    @XmlTransient
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "TEAM_MEETING_NAME")
    @XmlElement
    public String getTeam_meeting_name() {
        return team_meeting_name;
    }

    public void setTeam_meeting_name(String team_meeting_name) {
        this.team_meeting_name = team_meeting_name;
    }

    @Column(name = "YEAR")
    @XmlElement
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Column(name = "MEETING_NAME")
    @XmlElement
    public String getMeeting_name() {
        return meeting_name;
    }

    public void setMeeting_name(String meeting_name) {
        this.meeting_name = meeting_name;
    }

    @Column(name = "LINK")
    @XmlElement
    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

}
