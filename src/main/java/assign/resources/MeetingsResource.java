package assign.resources;

import assign.domain.Meetings;
import assign.domain.MeetingsCount;
import assign.services.JSoupHandlerServiceImpl;
import assign.services.JSoupService;
import assign.services.MeetingsService;
import assign.services.MeetingsServiceImpl;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Sara on 4/13/2015.
 */
@Path("/myeavesdrop/meetings")
public class MeetingsResource {

    MeetingsService meetingsService;
    JSoupService jSoupService;
    String password;
    String username;
    String dburl;

    public MeetingsResource(@Context ServletContext servletContext) {
        dburl = servletContext.getInitParameter("DBURL");
        username = servletContext.getInitParameter("DBUSERNAME");
        password = servletContext.getInitParameter("DBPASSWORD");
        this.meetingsService = new MeetingsServiceImpl();//dburl, username, password);
        this.jSoupService = new JSoupHandlerServiceImpl();
    }

    @GET
    @Path("/helloworld")
    @Produces("text/html")
    public String helloWorld() {
        System.out.println("Inside helloworld");
        System.out.println("DB creds are:");
        System.out.println("DBURL:" + dburl);
        System.out.println("DBUsername:" + username);
        System.out.println("DBPassword:" + password);
        return "Hello world " + dburl + " " + username + " " + password;
    }

    /*- Meeting name should be one of “solum” or “solum_team_meeting”.
        Return a 400 Bad Request response if some other meeting name is entered.
      - Year should be one of 2013, 2014, 2015.
        Return a 400 Bad Request response if some other year is entered.
    */

    /* The value of count will be all the meetings that happened in the
       specified year for that meeting name.
       Note that you need to obtain the count values by querying the database
       that you populated by running your ETL tool.
    GET /myeavesdrop/meetings/<name>/year/<year>/count
    Example 1: GET /myeavesdrop/meetings/solum/year/2014/count
    Response:
    <meetings>
        <count>1</count>
    </meetings>
    */

    @GET
    @Path("/{team_meeting_name}/year/{year}/count")
    @Produces("application/xml")
    public StreamingOutput getMeetingYearCount(@PathParam("team_meeting_name") String team_meeting_name,
                                               @PathParam("year") String year,
                                      @Context final HttpServletResponse response) throws Exception {
        //todo: need to query db for meeting/year
        //todo: if meeting exists then get count of meetings for that year
        //todo: create meetings count object, return that to user

        //todo: error checking name and year
        //meeting names: solum or solum_team_meeting
        //years: 2013, 2014, 2015
        if(!team_meeting_name.equals("solum") && !team_meeting_name.equals("solum_team_meeting")) {
            //try {
            System.out.println("Not a valid team meeting name");
            response.setStatus(400);
            //}
            //catch (Exception e){
            throw new BadRequestException();
            //}
        }
        if(!year.equals("2013") && !year.equals("2014") && !year.equals("2015")) {
            //try {
            System.out.println("Not a valid year");
            response.setStatus(400);
            //}
            //catch (Exception e){
            throw new BadRequestException();
            //}
        }

        MeetingsCount meetingsCount = new MeetingsCount();
        try {
            meetingsCount = meetingsService.getMeetingsYearCount(team_meeting_name, year);
        } catch (Exception e) {
            System.out.println(e);
        }
        final MeetingsCount finalMeetingsCount = meetingsCount;
        return new StreamingOutput() {
            public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                outputMeetingsCount(outputStream, finalMeetingsCount);
            }
        };
    }


    /*2) Get all the meetings that happened in a particular year for a particular project
    Example 1:
    GET /myeavesdrop/meetings/solum/year/2014/
    Response:
    <meetings>
        <meeting>
            <team_meeting_name>solum</team_meeeting_name>
            <year>2014</year>
            <meeting_name>solum.2014-07-29-16.03.log.html</meeting_name>
            <link>http://eavesdrop.openstack.org/meetings/solum/2014/solum.2014-07-29-16.03.log.html</link>
        <meeting>
    </meetings>*/
    @GET
    @Path("/{team_meeting_name}/year/{year}")
    @Produces("application/xml")
    public StreamingOutput getAllMeetingsForYear(@PathParam("team_meeting_name") String team_meeting_name,
                                               @PathParam("year") String year,
                                               @Context final HttpServletResponse response) throws Exception {
        //todo: error checking name and year
        if(!team_meeting_name.equals("solum") && !team_meeting_name.equals("solum_team_meeting")) {
            //try {
                System.out.println("Not a valid team meeting name");
                response.setStatus(400);
            //}
            //catch (Exception e){
                throw new BadRequestException();
            //}
        }
        if(!year.equals("2013") && !year.equals("2014") && !year.equals("2015")) {
            //try {
            System.out.println("Not a valid year");
            response.setStatus(400);
            //}
            //catch (Exception e){
            throw new BadRequestException();
            //}
        }


        Meetings meetings = new Meetings();
        try {
            meetings = meetingsService.getMeetingsForYear(team_meeting_name, year);
        } catch (Exception e) {
            System.out.println(e);
        }
        final Meetings finalMeetings = meetings;
        return new StreamingOutput() {
            public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                outputMeetings(outputStream, finalMeetings);
            }
        };
    }





    protected void outputMeetingsCount(OutputStream os, MeetingsCount meetingsCount) throws IOException {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(MeetingsCount.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(meetingsCount, os);
        } catch (JAXBException jaxb) {
            jaxb.printStackTrace();
            throw new WebApplicationException();
        }
    }

    protected void outputMeetings(OutputStream os, Meetings meetings) throws IOException {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Meetings.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(meetings, os);
        } catch (JAXBException jaxb) {
            jaxb.printStackTrace();
            throw new WebApplicationException();
        }
    }







    /*
    @GET
    @Path("/getprojecttest")
    @Produces("text/html")
    public String getProject() throws Exception {
        int project_id = 1;
        Project project = meetingsService.getProject(project_id);
        String name = project.getName();
        String projectDescription = project.getDescription();
        int projectId = project.getProjectId();
        return "Project name: " + name + "\nProject Description: " + projectDescription +
                "\nProject ID: " + projectId;
    }



    //todo: Create a project
    //POST http://localhost:8080/assignment5/myeavesdrop/projects/
   *//* Body:
    <project>
        <name>cs378</name>
        <description>Project representing cs378</description>
    </project>

    Response:
    If the request is successful:
       Response status: 201 Created
       Location header: <Full path to the newly created resource>
       Response body: Empty
    If the request is unsuccessful based on validation requirements:
       Response status: 400 Bad Request
       Response body: Empty
   *//*
    @POST
    @Path("/projects")
    @Consumes(MediaType.APPLICATION_XML)
    public void postProject(@Context final HttpServletResponse response, Project project) throws Exception {
        System.out.println("Project name from input: " + project.getName());
        System.out.println("Project description from input: " + project.getDescription());

        //create project in db table projects
        Project createdProject = meetingsService.addProject(project);
        int projectId = createdProject.getProjectId();
        if(projectId != -1) {
            String path = "http://localhost:8080/assignment5/myeavesdrop/projects/" + projectId;
            System.out.println("Path: " + path);
            //Response status: 201 Created, Location header: <Full path to the newly created resource>
            response.setStatus(201);
            response.setHeader("Location", path);
            try {
                response.flushBuffer();
            } catch (Exception e) {
                response.setStatus(400);
            }
        }
        else {
            response.setStatus(400);
        }
    }


    //todo: Update project
    *//*Update project example:
    PUT http://localhost:8080/assignment5/myeavesdrop/projects/<projectId>
    Body:
    <project>
        <name>cs378</name>
        <description>Updated description of cs378</description>
    </project>

    Response:
    If the request is successful:
       Response status: 204 No Content
       Response body: Empty
    If the request is unsuccessful based on validation requirements:
       Response status: 400 Bad Request
       Response body: Empty

    Can return the updated resource but not required*//*
    @PUT
    @Path("/projects/{projectID}")
    @Consumes("application/xml")
    public void putProject(@PathParam("projectID") String projectID, Project project,
                           @Context final HttpServletResponse response) throws Exception {
        if(project != null && project.getName().isEmpty() && project.getDescription().isEmpty()) {
            response.setStatus(400);
        }
        project.setProjectId(Integer.parseInt(projectID));
        Project updatedProject = meetingsService.updateProject(project);
        int projectId = updatedProject.getProjectId();
        if(projectId != 0) {
            response.setStatus(204);
            try {
                response.flushBuffer();
            } catch (Exception e) {
                response.setStatus(400);
            }
        }
        else {
            response.setStatus(400);
        }
    }

    //note: A meeting can be associated with only one project.

    //todo: Create a meeting for a project
    *//*POST http://localhost:8080/assignment5/myeavesdrop/projects/<projectId>/meetings
    Body:
    <meeting>
        <name>m1</name>
        <year>2014</year>
    </meeting>

    Response:
    If the request is successful:
       Response status: 201 Created
       Response body: Empty
    If the request is unsuccessful based on validation requirements:
       Response status: 400 Bad Request
       Response body: Empty
   *//*
    @POST
    @Path("/projects/{projectID}/meetings")
    @Consumes("application/xml")
    public void postMeetings(@PathParam("projectID") String projectID, Meeting meeting,
                             @Context final HttpServletResponse response) throws Exception {
        if(meeting != null && meeting.getName().isEmpty() && meeting.getYear().isEmpty()) {
            response.setStatus(400);
        }
        meeting.setProject_id(Integer.parseInt(projectID));
        Meeting createdMeeting = meetingsService.addMeeting(meeting);
        int meeting_id = createdMeeting.getMeeting_id();
        if(meeting_id != -1) {
            response.setStatus(201);
            try {
                response.flushBuffer();
            } catch (Exception e) {
                response.setStatus(400);
            }
        }
        else {
            response.setStatus(400);
        }

    }


    //todo: Get a project
*//*  GET http://localhost:8080/assignment5/myeavesdrop/projects/<projectId> (Links to an external site.)
    Return:
    <project id=projectId>
        <name>cs378</name>
        <description>Project representing cs378</description>
        <meetings>
            <meeting>
                <name>m1</name>
                <year>2014</year>
            </meeting>
            <meeting>
                <name>m2</name>
                <year>2015</year>
            </meeting>
        </meetings>
    </project>

    Response:
    If the request is successful:
        Response status: 200 OK
        Response body: XML representation of the requested resource
    If the request is unsuccessful based on validation requirements:
        Response status: 404 Not Found
        Response body: Empty
    *//*
    @GET
    @Path("/projects/{projectID}")
    @Produces("application/xml")
    public StreamingOutput getProject(@PathParam("projectID") String projectID,
                                      @Context final HttpServletResponse response) throws Exception {
        Project project = new Project();
        //project = projectMeetingsService.getProject(Integer.parseInt(projectID));

        try {
            project = meetingsService.getProject(Integer.parseInt(projectID));
            if(project == null) {
                System.out.println("Project does not exist for id = " + projectID);
                response.setStatus(404);
                throw new NotFoundException();
            }
            else {
                try {
                    List<Meeting> meetings = meetingsService.getMeetings(Integer.parseInt(projectID));
                    project.setMeetingList(meetings);
                } catch (Exception e) {
                    System.out.println("No meetings for this project id = " + projectID);
                    System.out.println("Exception: " + e);
                }
                //todo: output project and its meetings
                Project finalProject = project;
                return new StreamingOutput() {
                    public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                        outputProjects(outputStream, finalProject);
                    }
                };
            }
        } catch (Exception e) {
            System.out.println("Exception for project: " + projectID);
            response.setStatus(404);
            throw new NotFoundException();
        }

    }



    //todo: Delete a project and all the meetings associated with it
    //DELETE http://localhost:8080/assignment5/myeavesdrop/projects/<projectId> (Links to an external site.)
    *//*Response:
    If the request is successful:
        Response status: 200 OK
        Response body: Empty
    If the request is unsuccessful based on validation requirements:
        Response status: 404 Not Found
        Response body: Empty*//*
    @DELETE
    @Path("/projects/{projectID}")
    @Produces("application/xml")
    public void deleteProject(@PathParam("projectID") String projectID,
                              @Context final HttpServletResponse response) throws Exception {
        int projectResult = 0;
        int meetingsResult = 0;
        *//*meetingsResult = projectMeetingsService.deleteMeetings(Integer.parseInt(projectID));
        projectResult = projectMeetingsService.deleteProject(Integer.parseInt(projectID));*//*

        try {
            meetingsResult = meetingsService.deleteMeetings(Integer.parseInt(projectID));
        } catch (Exception e) {

        }
        try {
            projectResult = meetingsService.deleteProject(Integer.parseInt(projectID));
        } catch (Exception e) {
            response.setStatus(404);
        }


        //todo: deal with response
        if(projectResult != 0) {
            response.setStatus(200);
            try {
                response.flushBuffer();
            } catch (Exception e) {
                response.setStatus(404);
            }
        }
        else {
            response.setStatus(404);
        }
    }


*/

    /*protected void outputProjects(OutputStream os, Projects projects) throws IOException {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Projects.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(projects, os);
        } catch (JAXBException jaxb) {
            jaxb.printStackTrace();
            throw new WebApplicationException();
        }
    }

    protected void outputProjects(OutputStream os, Project project) throws IOException {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Project.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(project, os);
        } catch (JAXBException jaxb) {
            jaxb.printStackTrace();
            throw new WebApplicationException();
        }
    }

    protected void outputMeetings(OutputStream os, Meetings meetings) throws IOException {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Meetings.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(meetings, os);
        } catch (JAXBException jaxb) {
            jaxb.printStackTrace();
            throw new WebApplicationException();
        }
    }

    protected void outputMeetings(OutputStream os, Meeting meeting) throws IOException {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Project.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(meeting, os);
        } catch (JAXBException jaxb) {
            jaxb.printStackTrace();
            throw new WebApplicationException();
        }
    }*/
}

