package assign.resources;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Sara on 4/13/2015.
 */
@ApplicationPath("/assignment6")
public class MeetingsApplication extends Application {

    private Set<Object> singletons = new HashSet<Object>();
    private Set<Class<?>> classes = new HashSet<Class<?>>();

    public MeetingsApplication() {
    }

    @Override
    public Set<Class<?>> getClasses() {
        classes.add(MeetingsResource.class);
        return classes;
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}
