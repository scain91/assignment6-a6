package assign.resources;

import assign.domain.Course;
import assign.services.CourseStudentService;
import assign.services.CourseStudentServiceImpl;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

@Path("/ut")
public class UTCoursesResource {
	
	CourseStudentService courseStudentService;
	String password;
	String username;
	String dburl;
	
	public UTCoursesResource(@Context ServletContext servletContext) {		
		dburl = servletContext.getInitParameter("DBURL");
		username = servletContext.getInitParameter("DBUSERNAME");
		password = servletContext.getInitParameter("DBPASSWORD");
		this.courseStudentService = new CourseStudentServiceImpl(dburl, username, password);
	}
	
	@GET
	@Path("/helloworld")
	@Produces("text/html")
	public String helloWorld() {
		System.out.println("Inside helloworld");
		System.out.println("DB creds are:");
		System.out.println("DBURL:" + dburl);
		System.out.println("DBUsername:" + username);
		System.out.println("DBPassword:" + password);		
		return "Hello world " + dburl + " " + username + " " + password; 		
	}

    @GET
    @Path("/getcourse")
    @Produces("text/html")
    public String getCourse() throws Exception {
        int course_id = 1;
        Course course = courseStudentService.getCourse(course_id);
        String name = course.getName();
        String courseNum = course.getCourseNum();
        int courseId = course.getCourseId();
        return "Course name: " + name + " Course Num: " + courseNum + " Course ID: " + courseId;
    }
}