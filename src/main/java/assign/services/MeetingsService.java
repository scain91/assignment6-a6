package assign.services;

import assign.domain.Meetings;
import assign.domain.MeetingsCount;

/**
 * Created by Sara on 4/13/2015.
 */
public interface MeetingsService {
    MeetingsCount getMeetingsYearCount(String team_meeting_name, String year);

    Meetings getMeetingsForYear(String team_meeting_name, String year);






    /*public Project addProject(Project project) throws Exception;

    public Project getProject(int projectId) throws Exception;

    public Meeting addMeeting(Meeting meeting) throws Exception;

    public Meeting getMeeting(int meetingId) throws Exception;

    public Project updateProject(Project project) throws Exception;

    public List<Meeting> getMeetings(int projectId) throws Exception;

    int deleteProject(int projectId) throws Exception;

    int deleteMeetings(int projectId) throws Exception;*/
}
