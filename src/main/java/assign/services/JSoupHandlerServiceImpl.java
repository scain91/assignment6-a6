package assign.services;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.ListIterator;

public class JSoupHandlerServiceImpl implements JSoupService {

    public JSoupHandlerServiceImpl() {}

    @Override
	public Elements getElements(String source) {
	    Document doc = null;
		try {
			doc = Jsoup.connect(source).get();
		} catch (IOException e) {
			e.printStackTrace();
		}
	    Elements links = doc.select("body a");
        //Elements correctLinks = links.contains()
	    return links;
	}

    /*private static org.w3c.dom.Document convertStringToDocument(String xmlStr) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try
        {
            builder = factory.newDocumentBuilder();
            org.w3c.dom.Document doc = builder.parse( new InputSource( new StringReader( xmlStr ) ) );
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }*/

    @Override
    public Elements getElementsFromString(String element, String body) throws IOException, ParserConfigurationException, SAXException {
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        InputSource src = new InputSource();
        src.setCharacterStream(new StringReader(body));

        Document doc = (Document) builder.parse(src);
        /*String age = doc.getElementsByAttribute("age").item(0).getTextContent();
        String name = doc.getElementsByTagName("name").item(0).getTextContent();*/

        //Document doc = convertStringToDocument(body);
        /*try {
            doc = Jsoup.connect(source).get();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        Elements elements = doc.select(element);
        //Elements correctLinks = links.contains()
        return elements;
    }

    public ArrayList<String> getElementsExtensions(Elements elements) {
        ListIterator<Element> iter = elements.listIterator();
        ArrayList<String> projects = new ArrayList<String>();
        // Code path:
        //boolean seenParentDir = false;
        while(iter.hasNext()) {
            Element e = (Element) iter.next();
            String s = e.html();
            if (s != null) {
                projects.add(s);
                //s = s.replace("#", "%23");

                /*if (seenParentDir) {
                    System.out.println("s: " + s);
                    projects.add(s);
                } else if (!seenParentDir) {
                    if (s.contains("Parent Directory")) {
                        System.out.println("s: " + s);
                        seenParentDir = true;
                    }
                }*/
            }
        }
        return projects;
    }

    @Override
    public ArrayList<String> getLinkExtensions(Elements links) {
        ListIterator<Element> iter = links.listIterator();
        ArrayList<String> projects = new ArrayList<String>();
        // Code path:
        boolean seenParentDir = false;
        while(iter.hasNext()) {
            Element e = (Element) iter.next();
            String s = e.html();
            if (s != null) {
                s = s.replace("#", "%23");
                if (seenParentDir) {
                    System.out.println("s: " + s);
                    projects.add(s);
                } else if (!seenParentDir) {
                    if (s.contains("Parent Directory")) {
                        System.out.println("s: " + s);
                        seenParentDir = true;
                    }
                }
            }
        }
        return projects;
    }

    @Override
    public boolean projectExists(String origsource, String projectName) {
        Elements meetingslinks = getElements(origsource);
        ArrayList<String> possiblelinks = getLinkExtensions(meetingslinks);
        for(String l : possiblelinks) {
            if(l.equals(projectName)) {
                return true;
            }
        }
        return false;
    }


}