package assign.services;

import org.jsoup.select.Elements;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Sara on 3/13/2015.
 */
public interface JSoupService {
    public Elements getElements(String source);

    public ArrayList<String> getLinkExtensions(Elements links);

    public boolean projectExists(String origsource, String projectName);


    public Elements getElementsFromString(String element, String body) throws IOException, ParserConfigurationException, SAXException;

    public ArrayList<String> getElementsExtensions(Elements elements);

    }
