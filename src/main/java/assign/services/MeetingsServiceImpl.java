package assign.services;

import assign.domain.Meeting;
import assign.domain.Meetings;
import assign.domain.MeetingsCount;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Sara on 4/15/2015.
 */
public class MeetingsServiceImpl implements MeetingsService {
    private SessionFactory sessionFactory;

    Logger logger;

    public MeetingsServiceImpl() {
        // A SessionFactory is set up once for an application
        sessionFactory = new Configuration()
                .configure() // configures settings from hibernate.cfg.xml
                .buildSessionFactory();


        //logger = Logger.getLogger("EavesdropReader");
    }



    public Long addMeeting(String team_meeting_name, String year, String meeting_name, String link) throws Exception {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        Long meetingId = null;
        try {
            tx = session.beginTransaction();
            Meeting newMeeting = new Meeting( team_meeting_name, year, meeting_name, link);
            session.save(newMeeting);
            meetingId = newMeeting.getId();
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
                throw e;
            }
        }
        finally {
            session.close();
        }
        return meetingId;
    }

    //gets first meeting that matches the meeting_name
    public Meeting getMeetingByName(String meeting_name) throws Exception {
        Session session = sessionFactory.openSession();

        session.beginTransaction();

        Criteria criteria = session.createCriteria(Meeting.class).
                add(Restrictions.eq("meeting_name", meeting_name));

        List<Meeting> meetings = criteria.list();

        if (meetings.size() > 0) {
            return meetings.get(0);
        } else {
            return null;
        }
    }

    public Meeting getMeetingById(Long id) throws Exception {
        Session session = sessionFactory.openSession();

        session.beginTransaction();

        Criteria criteria = session.createCriteria(Meeting.class).
                add(Restrictions.eq("id", id));

        List<Meeting> meetings = criteria.list();

        if (meetings.size() > 0) {
            return meetings.get(0);
        } else {
            return null;
        }
    }

    public List<Meeting> getMeetingsByName(String meeting_name) throws Exception {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(Meeting.class).
                add(Restrictions.eq("meeting_name", meeting_name));

        List<Meeting> meetings = criteria.list();

        return meetings;
    }

    public List<Meeting> getMeetingsByTeamName(String team_meeting_name) throws Exception {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(Meeting.class).
                add(Restrictions.eq("team_meeting_name", team_meeting_name));

        List<Meeting> meetings = criteria.list();

        return meetings;
    }


    @Override
    public MeetingsCount getMeetingsYearCount(String team_meeting_name, String year) {
        MeetingsCount meetingsCount = new MeetingsCount();
        //todo: query database for meeting and year
        //todo: if it exists, count occurrences and add them to the object
        Session session = sessionFactory.openSession();

        session.beginTransaction();

        Criteria criteria = session.createCriteria(Meeting.class);
        Criterion cr_team_meeting_name = Restrictions.eq("team_meeting_name", team_meeting_name);
        Criterion cr_year = Restrictions.eq("year", year);
        LogicalExpression team_name_and_year_expr = Restrictions.and(cr_team_meeting_name, cr_year);
        criteria.add(team_name_and_year_expr);

        List<Meeting> meetingList = criteria.list();

        if (meetingList.size() > 0) {
            meetingsCount.setCount(meetingList.size());
            return meetingsCount;
        } else {
            return meetingsCount;
        }
    }

    /*Criteria cr = session.createCriteria(Employee.class);

    Criterion salary = Restrictions.gt("salary", 2000);
    Criterion name = Restrictions.ilike("firstNname","zara%");

    // To get records matching with OR condistions
    LogicalExpression orExp = Restrictions.or(salary, name);
    cr.add( orExp );

    // To get records matching with AND condistions
    LogicalExpression andExp = Restrictions.and(salary, name);
    cr.add( andExp );

    List results = cr.list();*/

    @Override
    public Meetings getMeetingsForYear(String team_meeting_name, String year) {
        Meetings meetings = new Meetings();
        //todo: query database for meeting and year
        //todo: if it exists, count occurrences and add them to the object

        Session session = sessionFactory.openSession();

        session.beginTransaction();

        Criteria criteria = session.createCriteria(Meeting.class);
        Criterion cr_team_meeting_name = Restrictions.eq("team_meeting_name", team_meeting_name);
        Criterion cr_year = Restrictions.eq("year", year);
        LogicalExpression team_name_and_year_expr = Restrictions.and(cr_team_meeting_name, cr_year);
        criteria.add(team_name_and_year_expr);

        List<Meeting> meetingList = criteria.list();

        if (meetingList.size() > 0) {
            meetings.setMeetings(meetingList);
            meetings.setCount(meetingList.size());
            return meetings;
        } else {
            return meetings;
        }
    }




    /*@Override
    public Project addProject(Project project) throws Exception {
        Connection conn = ds.getConnection();

        String insert = "INSERT INTO projects(name, project_description) VALUES(?, ?)";
        PreparedStatement stmt = conn.prepareStatement(insert,
                Statement.RETURN_GENERATED_KEYS);

        stmt.setString(1, project.getName());
        stmt.setString(2, project.getDescription());

        int affectedRows = stmt.executeUpdate();

        if (affectedRows == 0) {
            project.setProjectId(-1);
            throw new SQLException("Creating project failed, no rows affected.");
        }

        ResultSet generatedKeys = stmt.getGeneratedKeys();
        if (generatedKeys.next()) {
            project.setProjectId(generatedKeys.getInt(1));
        }
        else {
            project.setProjectId(-1);
            throw new SQLException("Creating project failed, no ID obtained.");
        }

        // Close the connection
        conn.close();

        return project;
    }

    @Override
    public Project getProject(int projectId) throws Exception {
        String query = "select * from projects where project_id=?";
        Connection conn = ds.getConnection();
        PreparedStatement s = conn.prepareStatement(query);
        s.setInt(1,projectId);
        ResultSet r = s.executeQuery();

        if (!r.next()) {
            return null;
        }

        Project p = new Project();
        p.setDescription(r.getString("project_description"));
        p.setName(r.getString("name"));
        p.setProjectId(r.getInt("project_id"));

        return p;
    }

    @Override
    public Project updateProject(Project project) throws Exception {
        String name = project.getName();
        String description = project.getDescription();
        int projectId = project.getProjectId();
        //UPDATE table_name SET field1=new-value1, field2=new-value2
        //[WHERE Clause]
        String query = "UPDATE projects SET name = ?, project_description = ? where project_id=" + projectId;

        Connection conn = ds.getConnection();
        //PreparedStatement s = conn.prepareStatement(query);
        PreparedStatement s = conn.prepareStatement(query,
                Statement.RETURN_GENERATED_KEYS);
        s.setString(1, project.getName());
        s.setString(2, project.getDescription());

        int r = s.executeUpdate();  //1 for success, 0 for failure
        System.out.println("r = " + r);
        *//*if (!r.next()) {
            return null;
        }

        Project p = new Project();
        p.setDescription(r.getString("project_description"));
        p.setName(r.getString("name"));
        p.setProjectId(r.getInt("project_id"));*//*

        return project;
    }


    @Override
    public Meeting addMeeting(Meeting meeting) throws Exception {
        Connection conn = ds.getConnection();

        String insert = "INSERT INTO meetings(name, year, project_id) VALUES(?, ?, ?)";
        PreparedStatement stmt = conn.prepareStatement(insert,
                Statement.RETURN_GENERATED_KEYS);

        stmt.setString(1, meeting.getName());
        stmt.setString(2, meeting.getYear());
        stmt.setInt(3, meeting.getProject_id());

        int affectedRows = stmt.executeUpdate();

        if (affectedRows == 0) {
            meeting.setMeeting_id(-1);
            throw new SQLException("Creating meeting failed, no rows affected.");
        }

        ResultSet generatedKeys = stmt.getGeneratedKeys();
        if (generatedKeys.next()) {
            meeting.setMeeting_id(generatedKeys.getInt(1));
        }
        else {
            meeting.setMeeting_id(-1);
            throw new SQLException("Creating project failed, no ID obtained.");
        }

        // Close the connection
        conn.close();

        return meeting;
    }

    @Override
    public Meeting getMeeting(int meetingId) throws Exception {
        String query = "select * from meetings where meeting_id=?";
        Connection conn = ds.getConnection();
        PreparedStatement s = conn.prepareStatement(query);
        s.setInt(1,meetingId);
        ResultSet r = s.executeQuery();

        if (!r.next()) {
            return null;
        }

        Meeting meeting = new Meeting();
        meeting.setYear(r.getString("year"));
        meeting.setName(r.getString("name"));
        meeting.setProject_id(r.getInt("project_id"));

        return meeting;

    }

    @Override
    public List<Meeting> getMeetings(int projectId) throws SQLException {
        String query = "select * from meetings where project_id=?";
        Connection conn = ds.getConnection();
        PreparedStatement s = conn.prepareStatement(query);
        s.setInt(1,projectId);
        ResultSet r = s.executeQuery();

        //count results
        String count = "select count(*) from meetings where project_id=?";
        PreparedStatement countstmt = conn.prepareStatement(count);
        countstmt.setInt(1,projectId);
        ResultSet rsize = countstmt.executeQuery();

        if(!rsize.next()) {
            System.out.println("No meetings found in getMeetings()");
            throw new SQLException();
        }
        while (rsize.next()) {
            System.out.println("rsize = " + rsize.getString(1));
        }

        if (!r.next()) {
            System.out.println("No meetings found in getMeetings()");
            throw new SQLException();
            //return null;
        }

        Meetings meetings = new Meetings();
        List<Meeting> meetingList = new ArrayList<Meeting>();
        System.out.println("r: " + r);
        while(r.next()) {
            System.out.println("inside r loop for getMeetings()");
            Meeting meeting = new Meeting();
            meeting.setYear(r.getString("year"));
            meeting.setName(r.getString("name"));
            meeting.setProject_id(r.getInt("project_id"));
            meeting.setMeeting_id(r.getInt("meetings_id"));
            System.out.println("meetings_id: " + r.getString("meetings_id"));
            System.out.println("project_id: " + r.getString("project_id"));

            meetingList.add(meeting);
        }
        meetings.setMeetings(meetingList);
        return meetingList;
    }

    @Override
    public int deleteProject(int projectId) throws Exception {
        String query = "DELETE from projects where project_id=?";
        Connection conn = ds.getConnection();
        PreparedStatement s = conn.prepareStatement(query);
        s.setInt(1,projectId);
        int r = s.executeUpdate();
        System.out.println("r value for delete projects = " + r);

        return r;
    }

    @Override
    public int deleteMeetings(int projectId) throws Exception {
        String query = "DELETE from meetings where project_id=?";
        Connection conn = ds.getConnection();
        PreparedStatement s = conn.prepareStatement(query);
        s.setInt(1,projectId);
        int r = s.executeUpdate();
        System.out.println("r value for delete meetings = " + r);


        return r;
    }*/
}
