package assign.etl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class Transformer {

    Logger logger;

    public Transformer() {
        logger = Logger.getLogger("Transformer");
    }

    public Map<String, List<String>> transform(Map<String, List<String>> data) {
        // Read the data;
        // transform it as required;
        // return the transformed data;
        //input: map<key:original url, value:List-of-parsed-entries>
        //original url: String source = "http://eavesdrop.openstack.org/meetings/solum/";
        //or original url: String source2 = "http://eavesdrop.openstack.org/meetings/solum_team_meeting/";
        // parsed entries like: solum.2014-07-29-16.03.log.html
        //output: map<absolute link, list<team_meeting_name, year, meeting_name>>


        logger.info("Inside transform.");

        Map<String, List<String>> newData = new HashMap<String, List<String>>();

        //todo: transform data into newData
        for(Map.Entry entry : data.entrySet()) {
            String[] splitUrl = entry.getKey().toString().split("/");
            String team_meeting_name = splitUrl[splitUrl.length-1];
            for(String str : (List<String>) entry.getValue()) {
                List<String> parsedValueList = new ArrayList<String>();
                String meeting_name = str;
                String link = entry.getKey().toString() + meeting_name;
                String[] getYearArray = str.replaceAll("[^a-zA-Z0-9\\s]", " ").split(" ");
                String year = ""; //getYearArray[1];//.substring(0,4);
                if(team_meeting_name.equals("solum")) {
                    year = getYearArray[1];
                }
                else {
                    year = getYearArray[3];
                }
                /*System.out.println("Year array:" );
                for(int i = 0; i < getYearArray.length; i++) {
                    System.out.println("" + i + ": " + getYearArray[i]);
                }*/
                //make object
                System.out.println("link: " + link);
                System.out.println("team_meeting_name: " + team_meeting_name);
                System.out.println("year: " + year);
                System.out.println("meeting_name: " + meeting_name);
                parsedValueList.add(team_meeting_name);
                parsedValueList.add(year);
                parsedValueList.add(meeting_name);
                newData.put(link, parsedValueList);
            }
        }

        return newData;
    }
}