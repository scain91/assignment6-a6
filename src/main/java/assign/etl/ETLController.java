package assign.etl;

import java.util.List;
import java.util.Map;

public class ETLController {

    EavesdropReader reader;
    Transformer transformer;
    DBLoader loader;

    public ETLController() {
        transformer = new Transformer();
        loader = new DBLoader();
    }

    public static void main(String[] args) {
        ETLController etlController = new ETLController();
        etlController.performETLActions();
    }

    private void performETLActions() {
        try {

            String source = "http://eavesdrop.openstack.org/meetings/solum/";
            String source2 = "http://eavesdrop.openstack.org/meetings/solum_team_meeting/";
            reader = new EavesdropReader(source);

            //get data from source
            // Read data
            //return map<this.url, List-of-parsed-entries>
            Map<String, List<String>> data = reader.readData();

            // Transform data
            //return map<absolute link, list<team_meeting_name, year, meeting_name>>
            Map<String, List<String>> transformedData = transformer.transform(data);

            // Load data
            //need to load data into mysql table
            //input: map<absolute link, list<team_meeting_name, year, meeting_name>>
            //team_meeting_name, year, meeting_name, link
            loader.loadData(transformedData);



            //get data from source2
            reader = new EavesdropReader(source2);

            // Read data2
            Map<String, List<String>> data2 = reader.readData();

            // Transform data2
            Map<String, List<String>> transformedData2 = transformer.transform(data2);

            // Load data2
            loader.loadData(transformedData2);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}