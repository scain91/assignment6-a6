package assign.etl;

import assign.services.JSoupHandlerServiceImpl;
import assign.services.JSoupService;
import com.google.common.collect.Maps;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class EavesdropReader {
    final String LOGHTML = ".log.html";
    final String LOGTXT = ".log.txt";
    final String HTML = ".html";
    final String TXT = ".txt";

    String url;
    Logger logger;
    JSoupService jSoupService;

    public EavesdropReader(String url) {
        this.url = url;
        jSoupService = new JSoupHandlerServiceImpl();
        logger = Logger.getLogger("EavesdropReader");
    }

    /*
     * Return a map where the contents of map is a single entry:
     * <this.url, List-of-parsed-entries>
     *     parsed entry example: solum.2014-07-29-16.03.log.html
     *     priority: .log.html -> .log.txt -> .html -> .txt
     */
    public Map<String, List<String>> readData() {

        logger.info("Inside readData.");

        Map<String, List<String>> data = new HashMap<String, List<String>>();

        //todo: Read and parse data from this.url
        //priority links: .log.html -> .log.txt -> .html -> .txt
        Elements yearElements = jSoupService.getElements(url);
        ArrayList<String> linkExtensionsYears = jSoupService.getLinkExtensions(yearElements);
        List<String> parsedEntries = new ArrayList<String>();

        //account for more than one log in each year
        for(String year : linkExtensionsYears) {
            Elements logLinkElements = jSoupService.getElements(url + "/" + year);
            ArrayList<String> logLinksExtensions = jSoupService.getLinkExtensions(logLinkElements);

            boolean added = false;
            HashMap<String, String> logMap = Maps.newHashMap();
            for(String log : logLinksExtensions) {
                String[] getlogArray = log.replaceAll("[^a-zA-Z0-9\\s]", " ").split(" ");
                String logDate = new String();
                if(getlogArray[1].equals("team")) {
                    //solum_team_meeting
                    for(int t = 3; t <= 7; t++) {
                        logDate += getlogArray[t];
                    }
                    System.out.println("solum_team_meeting logDate: " + logDate);
                }
                else {
                    //solum
                    for(int t = 1; t <= 5; t++) {
                        logDate += getlogArray[t];
                    }
                    System.out.println("solum logDate: " + logDate);
                }

                if(logMap.containsKey(logDate)) {
                    if(log.contains(LOGHTML)) {
                        logMap.remove(logDate);
                        logMap.put(logDate, log);
                        //System.out.println("added " + LOGHTML);
                    }
                    else if(log.contains(LOGTXT) && !logMap.get(logDate).contains(LOGHTML)) {
                        logMap.remove(logDate);
                        logMap.put(logDate, log);
                        //System.out.println("added " + LOGTXT);
                    }
                    else if(log.contains(HTML) && !log.contains(LOGHTML) &&
                            !logMap.get(logDate).contains(LOGHTML) &&
                            !logMap.get(logDate).contains(LOGTXT)) {
                        logMap.remove(logDate);
                        logMap.put(logDate, log);
                        //System.out.println("added " + HTML);
                    }
                    else if(log.contains(TXT) && !log.contains(LOGTXT) &&
                            !logMap.get(logDate).contains(LOGHTML) &&
                            !logMap.get(logDate).contains(LOGTXT) &&
                            !logMap.get(logDate).contains(HTML)) {
                        logMap.remove(logDate);
                        logMap.put(logDate, log);
                        //System.out.println("added " + TXT);
                    }
                }
                else {
                    logMap.put(logDate, log);
                    //System.out.println("added default");
                }
            }
            /*System.out.println("added:");
            if(logMap.containsKey(LOGHTML)) {
                parsedEntries.add(logMap.get(LOGHTML));
                System.out.println(logMap.get(LOGHTML));
                added = true;
            }
            else if(logMap.containsKey(LOGTXT) && !added) {
                parsedEntries.add(logMap.get(LOGTXT));
                System.out.println(logMap.get(LOGTXT));
                added = true;
            }
            else if(logMap.containsKey(HTML) && !added) {
                parsedEntries.add(logMap.get(HTML));
                System.out.println(logMap.get(HTML));
                added = true;
            }
            else if(logMap.containsKey(TXT) && !added) {
                parsedEntries.add(logMap.get(TXT));
                System.out.println(logMap.get(TXT));
                added = true;
            }*/
            for(Map.Entry entry : logMap.entrySet()) {
                parsedEntries.add(entry.getValue().toString());
                System.out.println("added: " + entry.getValue().toString());
            }

        }
        data.put(url,parsedEntries);
        return data;
    }
}