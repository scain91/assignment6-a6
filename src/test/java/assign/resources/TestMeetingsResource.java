package assign.resources;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.junit.Test;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import static junit.framework.Assert.assertTrue;

/**
 * Created by Sara on 4/28/2015.
 */
public class TestMeetingsResource {
   /* Test at least these:
    - GET /myeavesdrop/meetings/solum/year/2014/count

    - GET /myeavesdrop/meetings/solum_team_meeting/year/2013/count

    - GET /myeavesdrop/meetings/solum/year/2014/

    - GET /myeavesdrop/meetings/solum/year/2013/count
    */


    //    - GET /myeavesdrop/meetings/solum/year/2014/count
    @Test
    public void testGetMeetingYearCountSolum2014() {
        Client client = ClientBuilder.newClient();
        String xml = "";
        String url = "http://localhost:8080/assignment6/myeavesdrop/meetings/solum/year/2014/count";

        WebTarget target = client.target(url);
        Response response = target.request().get();
        String xmloutput = response.readEntity(String.class);
        if(response.getStatus() == 404) throw new WebApplicationException(Response.Status.NOT_FOUND);
        //assert status code is not 404
        //assert that there are correct number of meetings

        int meetingCount = 0;

        Document doc = Jsoup.parse(xmloutput, "", Parser.xmlParser());
        boolean solum_team_meeting_2013_count = false;
        for (Element e : doc.select("count")) {
            //System.out.println(e);
            if(e.text().contains("1")) {
                solum_team_meeting_2013_count = true;
            }
        }
        assertTrue(solum_team_meeting_2013_count);

        response.close();
    }

    //    - GET /myeavesdrop/meetings/solum_team_meeting/year/2013/count
    @Test
    public void testGetMeetingYearCountSolumTeamMeeting() {
        Client client = ClientBuilder.newClient();
        String xml = "";
        String url = "http://localhost:8080/assignment6/myeavesdrop/meetings/solum_team_meeting/year/2013/count";

        WebTarget target = client.target(url);
        Response response = target.request().get();
        String xmloutput = response.readEntity(String.class);
        if(response.getStatus() == 404) throw new WebApplicationException(Response.Status.NOT_FOUND);
        //assert status code is not 404
        //assert that there are correct number of meetings

        int meetingCount = 0;

        Document doc = Jsoup.parse(xmloutput, "", Parser.xmlParser());
        boolean solum_team_meeting_2013_count = false;
        for (Element e : doc.select("count")) {
            //System.out.println(e);
            if(e.text().contains("5")) {
                solum_team_meeting_2013_count = true;
            }
        }
        assertTrue(solum_team_meeting_2013_count);

        response.close();
    }

    //    - GET /myeavesdrop/meetings/solum/year/2014/
    @Test
    public void testGetAllMeetingsForYearSolum() {
        Client client = ClientBuilder.newClient();
        String xml = "";
        String url = "http://localhost:8080/assignment6/myeavesdrop/meetings/solum/year/2014";

        WebTarget target = client.target(url);
        Response response = target.request().get();
        String xmloutput = response.readEntity(String.class);
        if(response.getStatus() == 404) throw new WebApplicationException(Response.Status.NOT_FOUND);
        //assert status code is not 404
        //assert that there are correct number of meetings

        int meetingCount = 0;

        //todo: check that correct number of meetings were found and the right meetings were found
        Document doc = Jsoup.parse(xmloutput, "", Parser.xmlParser());
        boolean solum_team_meeting_2014_count = false;
        for (Element e : doc.select("count")) {
            //System.out.println(e);
            if(e.text().contains("1")) {
                solum_team_meeting_2014_count = true;
            }
        }

        boolean link = false;
        for (Element e : doc.select("link")) {
            //System.out.println(e);
            if(e.text().contains("http://eavesdrop.openstack.org/meetings/solum/solum.2014-07-29-16.03.log.html")) {
                link = true;
            }
        }
        assertTrue(solum_team_meeting_2014_count);
        assertTrue(link);

        response.close();
    }


    //    - GET /myeavesdrop/meetings/solum/year/2013/count
    @Test
    public void testGetAllMeetingsForYearSolum2013() {
        Client client = ClientBuilder.newClient();
        String xml = "";
        String url = "http://localhost:8080/assignment6/myeavesdrop/meetings/solum/year/2013/count";

        WebTarget target = client.target(url);
        Response response = target.request().get();
        String xmloutput = response.readEntity(String.class);
        if(response.getStatus() == 404) throw new WebApplicationException(Response.Status.NOT_FOUND);
        //assert status code is not 404
        //assert that there are correct number of meetings

        int meetingCount = 0;

        Document doc = Jsoup.parse(xmloutput, "", Parser.xmlParser());
        boolean solum_team_meeting_2013_count = false;
        for (Element e : doc.select("count")) {
            //System.out.println(e);
            if(e.text().contains("0")) {
                solum_team_meeting_2013_count = true;
            }
        }
        assertTrue(solum_team_meeting_2013_count);

        response.close();
    }


}
